import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stateful Widget Homework 3',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text('Stateful Widget Homework 3'),
          ),
          body:
          Container(
            padding: const EdgeInsets.all(0),
            child: FavoriteWidget(),
          )
      ),
    );
  }
}

class FavoriteWidget extends StatefulWidget {
  @override
  FavoriteWidgetState createState() => FavoriteWidgetState();
}

class FavoriteWidgetState extends State<FavoriteWidget> {
  String image1 = 'assets/lockOn.png';
  String image2 = 'assets/lockOff.png';
  String currentImage1 = 'assets/lockOn.png';
  String image3 = 'assets/beltWithoutLock.png';
  String image4 = 'assets/beltWithLockOff.png';
  String currentImage2 = 'assets/beltWithoutLock.png';
  @override
  Widget build(BuildContext context) {
      return Column(
        children: [
          _buildContainerRow(image1,image2,currentImage1,buttonText1),
          _buildContainerRow(image3,image4,currentImage2,buttonText2),
        ],
      );
  }
  Row _buildContainerRow(String img1,String img2, String cImg, Widget buttonText) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _buildButtonColumn(img1, img2, cImg, buttonText),
        Container(
          padding: EdgeInsets.all(8),
          child: Image.asset(
              cImg,
              width: 150,
          ),
        ),
      ],
    );
  }
  Column _buildButtonColumn(String img1, String img2,String cImg, Widget buttonText) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(8),
          child: IconButton(
            icon: Icon(Icons.play_circle_outline),
            iconSize: 64,
            color: Colors.deepOrange[500],
            onPressed: () => _changeImageState(img1,img2,cImg),
          ),
        ),
        Container(
          padding: EdgeInsets.all(8),
          child: buttonText,
        ),
      ],
    );
  }

  Widget buttonText1 = Container(
    padding: EdgeInsets.all(8),
    child: SizedBox(
      width: 128,
      child: Text(
        'Push the button on the side of the Lockseed to unlock it.'
      ),
    ),
  );

  Widget buttonText2 = Container(
    padding: EdgeInsets.all(8),
    child: SizedBox(
      width: 128,
      child: Text(
          'Secure the opened Lockseed onto the center of the Sengoku Driver.'
      ),
    ),
  );

  void _changeImageState(String img1, String img2, String currentImage) {
    setState(() {
      if (currentImage==img1) {
        currentImage = img2;
      } else {
        currentImage = img1;
      }
    });
  }
}